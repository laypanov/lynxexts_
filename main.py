import tempfile
from datetime import datetime
import os
import smtplib
import sys
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pyodbc
import xlsxwriter
import schedule
import time

from connection_pass import CONN_STRING, smtp_email, smtp_password
from sql import exec_xsl, exec_xml, select_events_task, select_eventtask_prepeare, update_events_task, \
    select_events_task_count, update_events_task_status
from templates_mail import status_change_body, status_change_title

import inspect
import logging

processed_flag = {'not_processed': 0, 'in_process': 1, 'processed_successfully': 2,
                  'processing_error': 3}

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")


# https://stackoverflow.com/a/11581118
def function_logger(file_level=1, console_level=None):
    function_name = inspect.stack()[1][3]
    logger = logging.getLogger(function_name)

    if logger.hasHandlers():
        return logger
    else:
        logger.setLevel(logging.DEBUG)
        logger.propagate = False
        console_level = 1
        file_level = 1

        if console_level is not None:
            ch = logging.StreamHandler()  # StreamHandler logs to console
            ch.setLevel(console_level)
            ch_format = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
            ch.setFormatter(ch_format)
            logger.addHandler(ch)

        fh = logging.FileHandler("logs/{0}.log".format("logs"))
        fh.setLevel(file_level)
        fh_format = logging.Formatter('%(asctime)s - %(lineno)d - %(levelname)-8s - %(name)s - %(message)s')
        fh.setFormatter(fh_format)
        logger.addHandler(fh)
    return logger


def start():
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    attachments_full_path = []
    events_task = get_events_task()
    if events_task:
        recid = events_task['RecID']
        typedoc = events_task['TypeDoc']
        nomdoc = events_task['NomDoc']
        attachments = True

        event_tp_results = get_etp(recid)
        if event_tp_results['Flag'] == processed_flag['in_process']:
            if attachments:
                attachments_full_path = set_attachments(typedoc, nomdoc)
                if attachments_full_path:
                    send_email_results = send_email(event_tp_results, attachments_full_path)
                    if send_email_results:
                        sql_etp_update(recid, processed_flag['processed_successfully'], attachments_full_path)
                    else:
                        sql_etp_update(recid, processed_flag['processing_error'], attachments_full_path)
                else:
                    sql_etp_update(recid, processed_flag['processing_error'], attachments_full_path)
        else:
            sql_etp_update(recid, event_tp_results['Flag'], attachments_full_path)
    else:
        f1_logger.error("Ошибка в предверительной обработке", exc_info=True)
        os.system('cp ./logs/logs.log ./logs/logs_{0}.log'.format(dt_string))
        f = open('./logs/logs.log', 'r+')
        f.truncate(0)


def set_attachments(typedoc, nomdoc):
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    try:
        if typedoc == 1:
            full_path_xls = create_xls(typedoc, nomdoc)
            if full_path_xls:
                full_path = {full_path_xls['xls']}
            else:
                return False
        elif typedoc == 2:
            full_path_xml = create_xml(typedoc, nomdoc)
            if full_path_xml:
                full_path = {full_path_xml['xml']}
            else:
                return False
        f1_logger.debug(full_path)
    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        return False
    return full_path


def get_events_task():
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    job_results = {}
    try:
        with DatabaseConnection(CONN_STRING) as cursor:
            results = cursor.execute(select_events_task)
            results_fetch = results.fetchone()

        if results_fetch:
            job_results = {
                'RecID': results_fetch.RecID,
                'Event_ID': results_fetch.Event_ID,
                'NomDoc': results_fetch.NomDoc,
                'TypeDoc': results_fetch.TypeDoc,
            }
            f1_logger.debug(job_results)
            f1_logger.debug('Установка статуса задачи в processed_flag[in_process]')
            with DatabaseConnection(CONN_STRING) as cursor:
                cursor.execute(update_events_task_status.format(flag=processed_flag['in_process'],
                                                                recid=job_results['RecID']))
    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        return False
    return job_results


def get_etp(recid):
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    try:
        with DatabaseConnection(CONN_STRING) as cursor:
            results = cursor.execute(select_eventtask_prepeare.format(recid))
            results_fetch = results.fetchone()

        job_results = {
            'Flag': results_fetch.Flag,
            'email': results_fetch.email,
            'Msg_Header': results_fetch.Msg_Header,
            'Msg_Body': results_fetch.Msg_Body,
            'Msg_Sign': results_fetch.Msg_Sign,
        }
        out_debug = '{Flag: %s, email: %s}' % (job_results['Flag'], job_results['email'])
        f1_logger.debug(out_debug)

        if job_results['Flag'] == processed_flag['in_process']:  # все ок
            f1_logger.info("Получены данные по задаче")
            return job_results
        elif job_results['Flag'] == 1:
            f1_logger.error("Задача обработана ранее")
        elif job_results['Flag'] == 2:
            f1_logger.error("Документ не найден")
        elif job_results['Flag'] == 3:
            f1_logger.error("Отсутствует email")
        elif job_results['Flag'] == 4:
            f1_logger.error("Задача не найдена")
        return job_results

    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        return False


def create_xml(typedoc, nomdoc):
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    try:
        with DatabaseConnection(CONN_STRING) as cursor:
            result = cursor.execute(exec_xml.format(typedoc, nomdoc)).fetchone()
        xml = result[0].encode()
        directory = "tmp/"
        file_name = 'xml_nomdoc_{0}.xml'.format(nomdoc)
        full_path = "".join("{0}{1}".format(directory, file_name))
        f = open(full_path, "wb")
        f.write(xml)
        f.close()
        f1_logger.debug(full_path)
    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        return False
    current_result = {'xml': full_path}
    return current_result


def create_xls(typedoc, nomdoc):
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    try:
        cell_size = []
        directory = "tmp/"
        file_name = 'xls_nomdoc_{0}.xlsx'.format(nomdoc)
        full_path = "".join("{0}{1}".format(directory, file_name))
        workbook = xlsxwriter.Workbook(full_path)
        worksheet = workbook.add_worksheet()

        header_format = workbook.add_format({'bold': True, 'font_name': 'Arial', 'font_size': 10})
        body_format = workbook.add_format({'font_name': 'Arial', 'font_size': 10, })

        with DatabaseConnection(CONN_STRING) as cursor:
            results = cursor.execute(exec_xsl.format(typedoc, nomdoc))
            results_fetch1 = results.fetchall()
            columns = [column for column in cursor.description]
            results.nextset()
            results_fetch2 = results.fetchone()

        for i, column in enumerate(columns):
            col = str(column[0]).strip()
            worksheet.write(0, i, col, header_format)
            len_column = len(col)
            worksheet.set_column(i, i, len_column)
            cell_size.append(len_column)

        for i, result in enumerate(results_fetch1):
            for j in range(len(result)):
                res = str(result[j]).strip()
                worksheet.write(i + 1, j, res, body_format)

                len_result = len(res)
                len_cell_size = cell_size[j]

                if len_cell_size <= len_result:
                    cell_size[j] = len_result
                    if len_result > 25:
                        len_result = len_result * 0.8
                    worksheet.set_column(j, j, len_result)
        workbook.close()

        current_result = {'xls': full_path}
        f1_logger.debug(full_path)
    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        return False
    return current_result


def send_email(event_tp_results, attachments):
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    fromaddr = ''
    toaddr = ''
    msg_header = event_tp_results['Msg_Header']
    msg_body = event_tp_results['Msg_Body']
    msg_sign = event_tp_results['Msg_Sign']

    try:
        t = tempfile.TemporaryFile()
        available_fd = t.fileno()
        t.close()
        os.dup2(2, available_fd)
        t = tempfile.TemporaryFile()
        os.dup2(t.fileno(), 2)

        msg = MIMEMultipart()
        server = smtplib.SMTP_SSL(host='', port=465, timeout=1000)
        server.login(smtp_email, smtp_password)

        rcpt = [toaddr] + [fromaddr]

        toaddr = '%s' % toaddr
        fromaddr = '%s' % fromaddr
        msg['to'] = toaddr
        msg['Subject'] = msg_header
        body = msg_body + msg_sign
        msg.attach(MIMEText(body, 'html', _charset='utf-8'))

        f1_logger.debug(attachments)
        for f in attachments or []:
            temp_file_name = f.replace('tmp/', "")
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=temp_file_name
                )
            part['Content-Disposition'] = 'attachment; filename="%s"' % temp_file_name
            msg.attach(part)
        text = msg.as_string()
        server.set_debuglevel(1)
        server.sendmail(fromaddr, rcpt, text)

        sys.stderr.flush()
        t.flush()
        t.seek(0)
        stderr_output = t.read()
        t.close()

        os.dup2(available_fd, 2)
        os.close(available_fd)

        for line in stderr_output.decode('utf-8').split("\n"):
            f1_logger.debug("{:3}".format(line))

        server.quit()
    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        return False
    return True


def get_template_email(order_info):
    template = False
    try:
        # счет подтвержден
        if order_info['Event_ID'] == 0:
            template_body = status_change_body.format(
                order_info['Document'],
                order_info['DateDoc'],
                order_info['status'])
            template_title = status_change_title.format(
                order_info['Document'],
                order_info['Summa'],
                order_info['DateDoc'],
                order_info['status'])
            template = {
                'template_title': template_title,
                'template_body': template_body,
            }
        if order_info['Event_ID'] == 1:
            template_body = status_change_body.format(
                order_info['Document'],
                order_info['DateDoc'],
                order_info['status'])
            template_title = status_change_title.format(
                order_info['Document'],
                order_info['Summa'],
                order_info['DateDoc'],
                order_info['status'])
            template = {
                'template_title': template_title,
                'template_body': template_body,
            }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(e, exc_type, fname, exc_tb.tb_line)
        return False
    return template


def sql_etp_update(recid, flag, attachments_full_path):
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    log_msg = []
    try:
        if flag != 1:
            with open("./logs/logs.log", 'r', encoding='utf-8') as f:
                log_msg = f.read().replace('\'', '')

        with DatabaseConnection(CONN_STRING) as cursor:
            cursor.execute(update_events_task.format(recid=recid, flag=flag, log_msg=log_msg))
            f1_logger.info("Запись в БД Events_Task")

        if flag == 1:
            for a in attachments_full_path:
                if a:
                    os.remove(a)
        f = open('./logs/logs.log', 'r+')
        f.truncate(0)
    except Exception:
        f1_logger.error("Exception occurred", exc_info=True)
        os.system('cp ./logs/logs.log ./logs/logs_%s_%s.log' % (recid, dt_string))
        f = open('./logs/logs.log', 'r+')
        f.truncate(0)


class DatabaseConnection(object):
    def __init__(self, connection_string):
        self.conn = pyodbc.connect(connection_string)
        self.conn.autocommit = True
        self.cursor = self.conn.cursor()

    def __enter__(self):
        return self.cursor

    def __exit__(self, *args):
        self.cursor.close()
        self.conn.close()


def get_count_job():
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    try:
        with DatabaseConnection(CONN_STRING) as cursor:
            results = cursor.execute(select_events_task_count)
            results_fetch = results.fetchone()
            if results_fetch[0] != 0:
                f1_logger.info("Кол-во необработанных записей в Events_Task: %s" % results_fetch[0])
        return results_fetch[0]
    except Exception:
        pass
        f1_logger.error("Exception occurred", exc_info=True)


def pre_start():
    f1_logger = function_logger(logging.DEBUG, logging.ERROR)
    count = get_count_job()
    i = 0
    while i < count:
        i = i + 1
        f1_logger.info("Обработка %s из %s \r\n" % (i, count))
        print("Обработка %s из %s" % (i, count))
        start()


if __name__ == '__main__':
    schedule.every(3).seconds.do(pre_start)
    while 1:
        schedule.run_pending()
        time.sleep(1)
